use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    return Ok(());
}

fn part1(input: &str) -> Result<i32, Box<Error>> {
    let mut sum: i32 = 0;

    // Parses every line and adds the frequency to the sum
    for line in input.lines() {
        let n: i32 = line.parse()?;
        sum += n
    }

    return Ok(sum);
}

fn part2(input: &str) -> Result<i32, Box<Error>> {
    let mut sum: i32 = 0;
    let mut seen = HashSet::new();
    // Adds the initial sum (0) to the seen sums
    seen.insert(0);

    // Loop because there's no guarantee that the first duplicated sum will be seen in the first for loop
    loop {
        // Parses every line and adds the frequency to the sum
        for line in input.lines() {
            let n: i32 = line.parse()?;
            sum += n;

            // Adds the sum to the seen sums
            // If it was already added the condition is true and that sum is returned
            if !seen.insert(sum) {
                return Ok(sum);
            }
        }
    }
}