use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let mut points: Vec<MovingPoint> = input.lines().filter_map(|line| MovingPoint::from(line)).collect();

    let mut prev_seen = false; // true if the last cycle didn't find anything
    let mut now_seen = false; // true if this cycle didn't find anything
    let mut i: u16 = 0; // Number of cycle
    let mut grid = Vec::new(); // Temp grid for printing

    while !prev_seen || now_seen {
        prev_seen = now_seen; // What was "now" before, is "prev" now

        // Get the boundaries
        let maxx = points.iter().map(|p| p.x).max().ok_or("Error finding maxx")?;
        let minx = points.iter().map(|p| p.x).min().ok_or("Error finding minx")?;
        let maxy = points.iter().map(|p| p.y).max().ok_or("Error finding maxy")?;
        let miny = points.iter().map(|p| p.y).min().ok_or("Error finding miny")?;

        if maxy - miny <= 12 { // Condition to show 
            now_seen = true; // Since we have shown

            let width = maxx - minx + 1;
            let height = maxy - miny + 1;

            // Clears the old grid and populates it with .
            grid.clear();
            for _ in 0..(width * height) {
                grid.push('.');
            }
            // Replace the dots with an # if there's a MovingPoint
            for point in points.iter() {
                grid[(point.x-minx + (point.y-miny) * width) as usize] = '#';
            }
            // Prints the result
            println!("part1:");
            for y in 0..height {
                let slice: String = grid[(y*width) as usize .. ((y+1)*width) as usize].iter().collect();
                println!("{}", &slice);
            }
            println!("part2: {}", i);
        }
        else {
            now_seen = false; // Since we haven't shown
        }

        // Moves the points
        for point in points.iter_mut() {
            point.next();
        }
        i += 1;
    }

    return Ok(());
}

// Local helper for a point that can move with fixed speed
#[derive(Clone)]
struct MovingPoint {
    x: i32,
    y: i32,
    dx: i32,
    dy: i32
}

impl MovingPoint {
    // MovingPoint from a string
    fn from(input: &str) -> Option<Self> {
        return Some(Self {
            x:  input[10..16].trim_start().parse().ok()?,
            y:  input[18..24].trim_start().parse().ok()?,
            dx: input[36..38].trim_start().parse().ok()?,
            dy: input[40..42].trim_start().parse().ok()?
        });
    }

    // Moves the point to the next location
    fn next(&mut self) {
        self.x += self.dx;
        self.y += self.dy;
    }
}
