use std::cmp::min;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let serial: i32 = read_to_string("./input/input")?.lines().nth(0).ok_or("No lines in input")?.parse()?;

    // Setup the partial sum of the power levels
    let mut levels = Vec::with_capacity(300*300);
    for y in 0..300 {
        for x in 0..300 {
            levels.push ( ((((x as i32 + 10) * y as i32 + serial) * (x as i32 + 10)) / 100 % 10) - 5
                + *get2d(&levels, x as i32 -1, y as i32).get_or_insert(0)
                + *get2d(&levels, x as i32, y as i32 - 1).get_or_insert(0)
                - *get2d(&levels, x as i32 -1, y as i32 - 1).get_or_insert(0)
            );
        }
    }

    println!("part1: {:?}", part1(&levels).ok_or("Error finding max")?);
    println!("part2: {:?}", part2(&levels).ok_or("Error finding max")?);

    return Ok(());
}

// Local helper to get an element from a 300x300 array
fn get2d(vec: &Vec<i32>, x: i32, y: i32) -> Option<i32> {
    if x < 0 || x >= 300 || y < 0 || y >= 300 {
        return None;
    }
    return Some(vec[(x + 300 * y) as usize]);
}

fn part1(levels: &Vec<i32>) -> Option<(i32, i32)> {
    let mut max_level = -46; // min of a level is -5; a 3x3 has a min of -5*9 = -45; -46 to be sure to find a minimum
    let mut solution = None; // the temp solution

    // Iterates over every top-left point of a 3x3 grid (so every point, except the 2 lines near the right and bottom border)
    for x in 0..298 {
        for y in 0..298 {
            // Gets the total power level of the 3x3 grid using the partial sums
            // A partial sum could not exist (for example a grid with top-left 0,0 doesn't have any partial sums before it)
            let level = 
                *get2d(levels, x-1, y-1).get_or_insert(0)
                + *get2d(levels, x+2, y+2).get_or_insert(0)
                - *get2d(levels, x+2, y-1).get_or_insert(0)
                - *get2d(levels, x-1, y+2).get_or_insert(0);
            // Replace the current solution
            if level > max_level {
                max_level = level;
                solution = Some((x, y));
            }
        }
    }
    return solution;
}

fn part2(levels: &Vec<i32>) -> Option<(i32, i32, i32)> {
    // Same as part1 but with variable square size
    let mut max_level = -5 * 300 * 300 - 1; // min of largest square (300x300)
    let mut solution = None;
    for x in 0..298 {
        for y in 0..298 {
            // Interates over possible sizes. Starts from 1 and ends with the distance between the point and the bottom/right border
            for size in 1..min(300-x,300-y) {
                let level = 
                    *get2d(levels, x-1, y-1).get_or_insert(0)
                    + *get2d(levels, x-1+size, y-1+size).get_or_insert(0)
                    - *get2d(levels, x-1+size, y-1).get_or_insert(0)
                    - *get2d(levels, x-1, y-1+size).get_or_insert(0);
                if level > max_level {
                    max_level = level;
                    solution = Some((x, y, size));
                }
            }
        }
    }
    return solution;
}