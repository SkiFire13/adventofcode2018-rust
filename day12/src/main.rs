use std::collections::VecDeque;
use std::iter::Iterator;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let (pots, rules) = parse_input(&input).ok_or("Error parsing input")?;

    println!("part1: {}", part1(&pots, &rules));
    println!("part2: {}", part2(&pots, &rules));

    return Ok(());
}


// Parses the input
fn parse_input(input: &str) -> Option<(Vec<Pot>, Vec<Rule>)> {
    let mut iter = input.lines();
    // Parses the initial state
    let pots = iter.next()?[15..].chars().filter_map(Pot::from).collect();
    // Parses the rules
    let rules = iter.skip(1).filter_map(|line| {
        let mut pots = line[..5].chars().filter_map(Pot::from);
        let start = [pots.next()?, pots.next()?, pots.next()?, pots.next()?, pots.next()?]; // Manual array of the 5 elements of pots; I didn't find any other (memory efficient) way to get a [Pot; 5] from an Iterator
        let end = Pot::from(line.chars().nth(9)?)?;
        Some(Rule::new(start, end))
    }).collect();

    return Some((pots, rules));
}

// Local helper for a Pot
#[derive(PartialEq, Eq, Clone, Copy)]
enum Pot {
    Plant,
    Nothing
}

impl Pot {
    // Parses a char into a Pot
    fn from(c: char) -> Option<Pot> {
        return match c {
            '#' => Some(Pot::Plant),
            '.' => Some(Pot::Nothing),
            _ => None
        };
    }
}

// Local helper for the rules
struct Rule {
    start: [Pot ; 5],
    end: Pot
}

impl Rule {
    fn new(start: [Pot ; 5], end: Pot) -> Rule {
        return Rule { start, end };
    }

    // Checks if the slice of 5 elements of vec, starting from start_index, matches the rule
    fn match_from(&self, vec: &VecDeque<Pot>, start_index: usize) -> bool {
        return self.start.iter().zip(vec.iter().skip(start_index).take(5)).all(|(&p1, &p2)| p1 == p2);
    }
}

// Normalize vec and updates offset for the normalized vec
fn normalize(vec: &mut VecDeque<Pot>, offset: &mut i64) {
    // Removes initial Nothing
    while let Some(Pot::Nothing) = vec.front() {
        vec.pop_front();
        *offset -= 1;
    }
    // Makes sure the vec starts with 5*Nothing and Plant
    for _ in 0..5 {
        vec.push_front(Pot::Nothing);
        *offset += 1;
    }
    // Removes ending Nothing
    while let Some(Pot::Nothing) = vec.back() {
        vec.pop_back();
    }
    // Makes sure the vec end with Plant and 5*Nothing
    for _ in 0..5 {
        vec.push_back(Pot::Nothing);
    }
}

fn next_pots(pots: &VecDeque<Pot>, rules: &Vec<Rule>, offset: &mut i64) -> VecDeque<Pot> {
    // Initialize a temp vec to store the pots
    let mut pots_temp = VecDeque::with_capacity(pots.len());

    // Iterates over each slice of 5 elements of pots. i is the index of the first element of the slice.
    // The last 4 indexes don't correspond to a slice of 5 elements and are skipped
    'i: for i in 0..pots.len()-4 {
        // Tries to match the 5-elements slice with a rule
        for rule in rules {
            if rule.match_from(&pots, i) {
                pots_temp.push_back(rule.end);
                continue 'i;
            }
        }
        // If a rule wasn't found, it defaults to Nothing
        pots_temp.push_back(Pot::Nothing);
    }

    // Since match_from on the first pot generates the third pot
    *offset -= 2;

    // Normalizes pots and updates offset
    // This is needed because pots should contain Nothing by default normalizing sets the first 5 and last 5 non-Plant elements to Nothing, enabling the match for them
    normalize(&mut pots_temp, offset);

    return pots_temp;
}

// Sums the pot index of every Plants, offsetted by offset since we should consider the negative indexes
fn sum_pots(pots: &VecDeque<Pot>, offset: i64) -> i64 {
    let mut sum = 0;
    for i in 0..pots.len() {
        if pots[i] == Pot::Plant {
            sum += i as i64 - offset;
        }
    }
    return sum;
}

fn part1(input_pots: &Vec<Pot>, rules: &Vec<Rule>) -> i64 {
    let mut pots: VecDeque<Pot> = input_pots.iter().map(|p| *p).collect();
    let mut offset = 0;

    // Normalized the pots. This is needed because the next_pots function does it only at the end
    normalize(&mut pots, &mut offset);

    // Generates the next 20 pots
    for _ in 0..20 {
        pots = next_pots(&pots, rules, &mut offset);
    }

    return sum_pots(&pots, offset);
}

// Almost the same as part1
fn part2(input_pots: &Vec<Pot>, rules: &Vec<Rule>) -> i64 {
    let mut pots: VecDeque<Pot> = input_pots.iter().map(|p| *p).collect();
    let mut offset = 0;

    normalize(&mut pots, &mut offset);

    // Iterates over the mext 50_000_000_000 (fifthy billions) pots
    // Actually it will detect a certain pattern and end before
    for i in 0..50_000_000_000u64 {
        // We save the old offset to compare it with the new one in case there's a pattern
        let old_offset = offset;
        let new_pots = next_pots(&pots, rules, &mut offset);

        // Checks if the pots are equal, without checking the offset
        if new_pots == pots {
            // If the pots are equal (except the offset), there's a pattern where each iteration the offset changes in a linear way (since it's bound to pots, and pots remains the same)

            // We increment the offset by the delta multiplied by the number of iteration we still need
            offset += (offset - old_offset) * ( 50_000_000_000i64 - (i+1) as i64);

            return sum_pots(&pots, offset);
        }

        pots = new_pots;
    }

    return sum_pots(&pots, offset);
}
