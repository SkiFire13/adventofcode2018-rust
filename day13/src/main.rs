use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let (grid, carts) = parse_input(&input).ok_or("No input")?;

    println!("part1: {:?}", part1(&grid, carts.clone()));
    println!("part2: {:?}", part2(&grid, carts.clone()));

    return Ok(());
}

fn parse_input(input: &str) -> Option<(Grid, Vec<Cart>)> {
    // Converts the input into a grid
    let grid = Grid {
        vec: input.lines().flat_map(|line| line.chars()).collect(),
        width: input.lines().nth(0)?.len(),
        height: input.lines().count()
    };

    // Interates over each char of the grid and if it's cart, parses it
    let mut carts = Vec::new();
    for y in 0..grid.height {
        for x in 0..grid.width {
            match grid.get(x, y) {
                'v' => carts.push(Cart::new(x + grid.width * y, x, y, Direction::DOWN)),
                '^' => carts.push(Cart::new(x + grid.width * y, x, y, Direction::UP)),
                '<' => carts.push(Cart::new(x + grid.width * y, x, y, Direction::LEFT)),
                '>' => carts.push(Cart::new(x + grid.width * y, x, y, Direction::RIGHT)),
                _ => {}
            }
        }
    }

    return Some((grid, carts));
}

// Helper function for a grid
struct Grid {
    vec: Vec<char>,
    width: usize,
    height: usize
}
impl Grid {
    fn get(&self, x: usize, y: usize) -> char {
        return self.vec[x + self.width * y];
    }
}

// 
#[derive(Clone)]
struct Cart {
    id: usize, // Initial position of the cart, used to differentiate two carts with the same coordinate 
    x: usize,
    y: usize,
    direction: Direction,
    count: u32, // Counts how many times the cart met a crossroad
    crashed: bool
}
impl Cart {
    fn new(id: usize, x: usize, y: usize, direction: Direction) -> Self {
        return Self { id, x, y, direction, count: 0, crashed: false };
    }
    // fn new_with_count(x: usize, y: usize, direction: Direction, count: u32) -> Self {
    //     return Self { x, y, direction, count, crashed: false};
    // }
    fn next(&mut self, grid: &Grid) {
        // Updates the coordinates
        self.x = (self.x as i32 + self.direction.dx()) as usize;
        self.y = (self.y as i32 + self.direction.dy()) as usize;
        // Updates the direction
        match grid.get(self.x, self.y) {
            // Straight direction
            '-' | '|' | 'v' | '^' | '<' | '>' => {},
            // Turn left or right, depending on where it's coming from
            '/' => match self.direction {
                Direction::UP | Direction::DOWN => self.direction = self.direction.turn_right(),
                Direction::LEFT | Direction::RIGHT => self.direction = self.direction.turn_left()
            },
            // Turn left or right, depending on where it's coming from
            '\\' => match self.direction {
                Direction::UP | Direction::DOWN => self.direction = self.direction.turn_left(),
                Direction::LEFT | Direction::RIGHT => self.direction = self.direction.turn_right()
            },
            // Crossroad, go straight, turn left or turn right depending on the times this cart has met a crossroad
            '+' => {
                match self.count % 3 {
                    0 => self.direction = self.direction.turn_left(),
                    2 => self.direction = self.direction.turn_right(),
                    _ => {}
                }
                self.count += 1;
            }
            // Something went wrong
            c => panic!("Invalid char {} at ({}, {})", c, self.x, self.y)
        };
    }
}

// Local helper to describe a direction
#[derive(Clone)]
enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
impl Direction {
    // Gets the variation in x when following self
    fn dx(&self) -> i32 {
        return match self {
            Direction::LEFT => -1,
            Direction::RIGHT => 1,
            _ => 0
        };
    }
    // Gets the variation in y when following self
    fn dy(&self) -> i32 {
        return match self {
            Direction::UP => -1,
            Direction::DOWN => 1,
            _ => 0
        };
    }

    // Gets the direction to the right of self
    fn turn_right(&self) -> Direction {
        return match self {
            Direction::UP => Direction::RIGHT,
            Direction::RIGHT => Direction::DOWN,
            Direction::DOWN => Direction::LEFT,
            Direction::LEFT => Direction::UP
        };
    }
    // Gets the direction to the left of self
    fn turn_left(&self) -> Direction {
        return match self {
            Direction::UP => Direction::LEFT,
            Direction::LEFT => Direction::DOWN,
            Direction::DOWN => Direction::RIGHT,
            Direction::RIGHT => Direction::UP
        };
    }
}

// Generates the next tick, mutating `carts`
fn next_tick(grid: &Grid, carts: &mut Vec<Cart>) {
    // Interates over the elements of carts
    // We don't use iter or iter_mut because we want to mutate other elements of carts inside the loop
    for i in 0..carts.len() {
        // Gets the current cart and updates it
        let cart = &mut carts[i];
        if !cart.crashed {
            cart.next(grid);
        }
        // Copy id, x and y of cart
        // Needed because cart is a mutable reference and won't let us use iter_mut if we don't get rid of it
        let (id, x, y) = (cart.id, cart.x, cart.y);
        let mut crashed = false;
        // Filter the carts that crashed with the current one and update them
        for c in carts.iter_mut().filter(|c| c.x == x && c.y == y && c.id != id) {
            c.crashed = true;
            crashed = true;
        }
        // If any cart has crashed we update the current one as crashed too
        // We need to get a new mutable reference because we got rid of `cart`
        carts[i].crashed = crashed;
    }
}

fn part1(grid: &Grid, mut carts: Vec<Cart>) -> (usize, usize) {
    // Generate the next tick until one cart crashed
    while carts.iter().all(|c| !c.crashed) {
        next_tick(grid, &mut carts);
    }
    // Get the first that crashed (should have the same coordinates as the other one)
    return carts.iter().filter(|c| c.crashed).map(|c| (c.x, c.y)).next().unwrap();
}

fn part2(grid: &Grid, mut carts: Vec<Cart>) -> (usize, usize) {
    // Generate the next tick until only one cart remains
    while carts.len() > 1 {
        next_tick(grid, &mut carts);
        // Filters the carts that crashed and sorts the others
        carts = carts.into_iter().filter(|c| !c.crashed).collect();
        carts.sort_by(|a, b| a.y.cmp(&b.y).then(a.x.cmp(&b.x)));
    }
    // Get the last cart and returns it
    return carts.iter().filter(|c| !c.crashed).map(|c| (c.x, c.y)).next().unwrap();
}
