use std::error::Error;
use std::fs::read_to_string;

const CHUNK_SIZE: usize = 20_000_000; // Size of each chunk part2 will generate before trying to find the input

fn main() -> Result<(), Box<Error>> {
    let input: usize = read_to_string("./input/input")?.lines().nth(0).ok_or("Invalid input")?.parse()?;
    // We need input+10 for part1 and CHUNK_SIZE for part2
    // Since the last step may generate 2 digits, we need 1 more slot for each part (from here the +2)
    let mut scoreboard = RecipeScoreboard::new(input + 10 + CHUNK_SIZE + 2);

    println!("part1: {}", part1(&input, &mut scoreboard));
    println!("part2: {}", part2(&input, &mut scoreboard));

    return Ok(());
}

// Local helper for the scoreboard
struct RecipeScoreboard {
    elf1: usize,
    elf2: usize,
    recipes: Vec<u8>
}
impl RecipeScoreboard {
    // Initializes a new RecipeScoreboard, giving recipes an initial capacity for better performance
    fn new(max: usize) -> Self {
        let mut recipes = Vec::with_capacity(max);
        // Adds initial values
        recipes.push(3);
        recipes.push(7);
        return Self { elf1: 0, elf2: 1, recipes };
    }

    // Updates the scoreboard with the next recipes
    fn next(&mut self) {
        // Gets the sum of the current recipes
        let sum: u8 = self.recipes[self.elf1] + self.recipes[self.elf2];
        // If the sum has 2 digit it must be in the form '1x' so we can optimize this step
        if sum >= 10 {
            self.recipes.push(1);
            self.recipes.push(sum % 10);
        }
        // If the sum doesn't have 2 digits, it must have a single digit. Just pushes this into the vec
        else {
            self.recipes.push(sum);
        }
        // Updates the elfs current recipes
        self.elf1 = ( self.elf1 + 1 + self.recipes[self.elf1] as usize ) % self.recipes.len();
        self.elf2 = ( self.elf2 + 1 + self.recipes[self.elf2] as usize ) % self.recipes.len();
    }
}

fn part1(input: &usize, scoreboard: &mut RecipeScoreboard) -> String {
    // Since we want the 10 recipes after the first `input` recipes, we know we need at least input + 10 recipes
    let num_recipes = input + 10;
    // Generates at least num_recipes recipes
    while scoreboard.recipes.len() < num_recipes {
        scoreboard.next();
    }
    return scoreboard.recipes.iter().skip(*input).take(10).map(|i| i.to_string()).collect();
}

// Note: This runs in about 10 seconds which feels a bit too slow
fn part2(input: &usize, scoreboard: &mut RecipeScoreboard) -> usize {
    // Converts the input into a [u8] with its digits to speed up the search in the recipes
    let to_find: Vec<u8> = input.to_string().chars().map(|d| d.to_digit(10).unwrap() as u8).collect();
    // The loop is needed since we generate CHUNK_SIZE steps and then search for the input but it may not be present
    loop {
        // Caches the wanted length of recipes
        // We don't loop CHINK_SIZE times because each iteration may add 1 or 2 digits, which may exceed the capacity
        let target_length = scoreboard.recipes.len() + CHUNK_SIZE;
        while scoreboard.recipes.len() < target_length {
            scoreboard.next();
        }
        // Search for the input into the recipes and if found returns it
        if let Some(index) = scoreboard.recipes.windows(to_find.len()).position(|window| to_find == window) {
            return index;
        }
        // If the input wasn't found we repeat the loop, adding more capacity
        scoreboard.recipes.reserve(CHUNK_SIZE + 1);
    }
}
