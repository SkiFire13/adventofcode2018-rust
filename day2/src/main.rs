use std::collections::HashMap;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input).ok_or("No box id found")?);

    return Ok(());
}

fn part1(input: &str) -> u32 {
    let mut count_two: u32 = 0;
    let mut count_three : u32 = 0;

    // Interates over every line, counting if the word has a letter repeated two or three times
    for line in input.lines() {
        // Counter for the letters
        let mut counter = HashMap::new();
        for c in line.chars() {
            *counter.entry(c).or_insert(0) += 1;
        }
        if counter.values().any(|c| *c == 2) { count_two += 1; }
        if counter.values().any(|c| *c == 3) { count_three += 1; }
    }

    return count_two * count_three;
}

fn part2(input: &str) -> Option<String> {
    // Iterates over each line
    for (i, line) in input.lines().enumerate() {
        // Iterates over each line again
        // The first i lines are excluded because already checked
        // The i+1 line is the current one from the first for loop so also excluded
        for other in input.lines().skip(i+1) {
            // String of chars equal between 'line' and 'other'
            let same_chars: String = line
                .chars()
                .zip(other.chars())
                .filter_map(|(a,b)| { if a == b { Some(a) } else { None } })
                .collect();
            // If same_chars has length one less than line, then only one char was different
            if same_chars.len() == line.len() - 1 {
                // Returning same_chars since it's already the initial string without the different char
                return Some(same_chars)
            }
        }
    }
    return None;
}