extern crate regex;

use regex::Regex;
use std::cmp::max;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let claims = Claim::vec_from(&read_to_string("./input/input")?)?;
    let claim_count = get_claim_count(&claims);

    println!("part1: {}", part1(&claim_count));
    println!("part2: {}", part2(&claims, &claim_count).ok_or("No claim found")?);

    return Ok(());
}

// Represent a region claim. It's basically a rectangle with an id.
struct Claim {
    id: u16,
    x: usize, // x distance from origin
    y: usize, // y distance from origin
    width: usize, // width of the rectangle
    height: usize // height of rectange
}

impl Claim {
    // Parses the input and returns a list of claims if successful
    fn vec_from(input: &str) -> Result<Vec<Self>, Box<Error>> {
        Ok(
            Regex::new(r"(?m)#(?P<id>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<width>\d+)x(?P<height>\d+)")?
                .captures_iter(input)
                .map(|caps| -> Result<Self, Box<Error>> {
                    Ok(Self {
                        id: caps["id"].parse()?,
                        x: caps["x"].parse()?,
                        y: caps["y"].parse()?,
                        width: caps["width"].parse()?,
                        height: caps["height"].parse()?
                    })
                })
                .filter_map(Result::ok)
                .collect()
        )
    }
}

// Local helper for a 2D Vec of u16
struct Vec2D {
    vec: Vec<u16>,
    width: usize
}

// Returns a 2D vector containing the number of claims for each point
fn get_claim_count(claims: &Vec<Claim>) -> Vec2D {
    // Obtain the furthest point claimed
    let (tot_width, tot_height) = claims
        .iter()
        .fold((0, 0), |(old_w, old_h), claim|
            ( max(old_w, claim.x + claim.width), max(old_h, claim.y + claim.height) )
        );

    let mut claim_count = vec![0u16; tot_height * tot_width];

    // Iterates over the claims
    for claim in claims.iter() {
        // Iterates over each point of the claim
        for x in claim.x..(claim.x + claim.width) {
            for y in claim.y..(claim.y + claim.height) {
                // Increments the number of claims of that point
                claim_count[x + y * tot_width] += 1;
            }
        }
    }

    return Vec2D { 
        vec: claim_count,
        width: tot_width
    };
}

fn part1(claim_count: &Vec2D) -> usize {
    return claim_count.vec.iter().filter(|count| **count >= 2).count();
}

fn part2(claims: &Vec<Claim>, claim_count: &Vec2D) -> Option<u16> {
    // Iterates over the claims
    'claim: for claim in claims.iter() {
        // Iterates over each point of the claim
        for x in claim.x..(claim.x + claim.width) {
            for y in claim.y..(claim.y + claim.height) {
                // Check if every point has only one claim (the current claim)
                // If there is more than one claim, checks the next claim
                if claim_count.vec[x + y * claim_count.width] > 1 {
                    continue 'claim;
                }
            }
        }
        // If it checked every point and didn't continue, then all of them have only one claim
        // Return then the id of the claim find
        return Some(claim.id);
    }
    return None;
}
