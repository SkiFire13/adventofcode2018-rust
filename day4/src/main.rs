extern crate itertools;

use itertools::Itertools;
use std::collections::HashMap;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let hours = log_to_hours(&parse_logs()?)?;

    println!("part1: {}", part1(&hours)?);
    println!("part2: {}", part2(&hours)?);

    return Ok(());
}

// Describes an event from the logs
#[derive(Debug)]
enum LogEvent {
    GuardChange(u16), // The guard has changed
    GuardWakesUp(u16), // The guard wakes up
    GuardSleeps(u16) // The guard sleeps
}

impl LogEvent {
    // Parses the string and return a Log Event if successful
    fn from(s: &str) -> Option<Self> {
        if s.contains("Guard") { return Some(LogEvent::GuardChange( s[26..s.find(" b")?].parse().ok()? )); }

        // minute is common between falls and wakes, so we can precompute it after checking it's not a GuardChange
        let minute: u16 = s[15..17].parse().ok()?;
        if s.contains("falls") { return Some(LogEvent::GuardSleeps( minute )); }
        if s.contains("wakes") { return Some(LogEvent::GuardWakesUp( minute )); }

        return None;
    }
}

// Parse the logs and returns the list of LogEvent
fn parse_logs() -> Result<Vec<LogEvent>, Box<Error>>  {
    return Ok(read_to_string("./input/input")?
                .lines()
                .sorted() // Necessary for sorting chronological order
                .filter_map(|line| LogEvent::from(line))
                .collect()
            );
}

// Takes the list of events and returns a map between the guard id and a list with the number of times he slept in that minute
fn log_to_hours(logs: &[LogEvent]) -> Result<HashMap<u16, [u16; 60]>, Box<Error>> {
    // First event should be a GuardChange. Return if not
    let mut current_id = match logs[0] {
        LogEvent::GuardChange(id) => id,
        _ => return Err(From::from("First event is not GuardChange"))
    };

    // Second event should be a GuardSleeps. Return if not
    let mut asleep_minute = match logs[1] {
        LogEvent::GuardSleeps(minute) => minute,
        _ => return Err(From::from("Second event is not GuardSleeps"))
    };

    let mut map: HashMap<u16, [u16; 60]> = HashMap::new();

    // Iterating over the logs, skipping the first 2 that were already taken care of
    for log in logs.iter().skip(2) {
        match log {
            LogEvent::GuardChange(id) => current_id = *id, // If the the guard change, change the current_id
            LogEvent::GuardSleeps(minute) => asleep_minute = *minute, // Saves the minute the guard went asleep, for future uses
            LogEvent::GuardWakesUp(minute) =>
                // For each minute asleep, add 1 to the corresponding minute in the map
                for i in asleep_minute..*minute {
                    map.entry(current_id).or_insert([0u16; 60])[i as usize] += 1;
                }
        }
    }

    return Ok(map);
}

fn part1(hours: &HashMap<u16, [u16; 60]>) -> Result<u32, Box<Error>> {
    // Finds the id of most asleep guard
    let (id, _) = hours
        .iter()
        .max_by_key(|(_, h)| h.iter().sum::<u16>()) // Max by the sum of minutes asleep
        .ok_or("No max guard found")?;
    // Finds the minute the guard was most asleep
    let (minute, _) = hours[id]
        .iter()
        .enumerate() // The index from enumerate is the number of the minute
        .max_by_key(|(_, m)| **m) // Max by the time the guard was asleep in that minute
        .ok_or("No max hour found")?;
    return Ok(minute as u32 * *id as u32);
}

fn part2(hours: &HashMap<u16, [u16; 60]>) -> Result<u32, Box<Error>> {
    let (id, (minute, _)) = hours
        .iter()
        // Map with max number of minutes asleep
        .map(|(id, minutes)| ( *id, minutes.iter().enumerate().max_by_key(|(_, m)| *m).unwrap() )) 
        .max_by_key(|(_, (_, m))| *m) // Max of guards by the max number of minutes asleep
        .ok_or("No max hour found")?;
    return Ok(id as u32 * minute as u32);
}
