use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let data = input.lines().next().ok_or("Invalid input")?;

    println!("part1: {}", part1(&data));
    println!("part2: {}", part2(&data).ok_or("Error while filtering chars")?);

    return Ok(());
}

fn collapse(input: &str) -> usize {
    // Checks if two chars are the same but with different case.
    fn are_opposite(c1: &char, c2: &char) -> bool {
        return c1 != c2 && c1.to_ascii_lowercase() == c2.to_ascii_lowercase();
    }

    let mut stack = Vec::<char>::new();

    // Iterates over the input chars
    for c in input.chars() {
        // If the stack is empty just push and continue
        if stack.is_empty() {
            stack.push(c);
        }
        // If the stack is not empty, compare with the last element of the stack.
        // If they're opposite, they cancel each other, so remove the last from the stack and ignore the current char
        else if are_opposite(&c, stack.last().unwrap()) {
            stack.pop();
        }
        // If they're not opposite, just add the new char to the stack
        else {
            stack.push(c);
        }
    }

    // The remaning chars all in the stack. Returns the length since we only need that.
    return stack.len();
}

fn part1(input: &str) -> usize {
    // Just calls collapse with the input
    return collapse(input);
}

fn part2(input: &str) -> Option<usize> {
    return input
        .chars()
         // Maps each character to a tuple with lowercase and uppercase variant (the latter one as cache)
        .map(|c| ( c.to_ascii_lowercase(), c.to_ascii_uppercase() ) )
        // Collects in an HashSet removing duplicates. It would be worse executing collapse for every duplicate
        .collect::<HashSet<_>>()
        .iter()
        .map(|(c_lower, c_upper)|
            // Calls collapse on the input without the characters.
            collapse(
                &input
                    .chars()
                    // Here we use the cached uppercase variant instead of calculating it for every check
                    .filter(|x| x != c_lower && x != c_upper) 
                    .collect::<String>()
            )
        )
        .min();
}
