use std::collections::HashSet;
use std::error::Error;
use std::fs::read_to_string;
use std::iter::FromIterator;

fn main() -> Result<(), Box<Error>> {
    // Parses the input in a list of Points
    let points: Vec<Point> = read_to_string("./input/input")?
        .lines()
        .filter_map(|line| Point::from(line))
        .collect();

    println!("part1: {}", part1(&points)?);
    println!("part2: {}", part2(&points)?);

    return Ok(());
}

// Local helper for point
#[derive(PartialEq, Eq, Hash, Clone)]
struct Point {
    x: usize,
    y: usize
}

impl Point {
    // Parses a point from a single-line string
    fn from(line: &str) -> Option<Self> {
        let i = line.find(", ")?;
        return Some(Point { x: line[..i].parse().ok()?, y: line[i+2..].parse().ok()? });
    }

    // Calculates the Manhattan distance between this point and a point with coordinates (x, y)
    fn dist(&self, x: usize, y: usize) -> usize {
        return ( (self.x as i16 - x as i16).abs() + (self.y as i16 - y as i16).abs() ) as usize;
    }
}

// Finds the closest point to a point of coordinates (x, y)
// Returns None if there's a tie
fn closest(points: &Vec<Point>, x: usize, y: usize) -> Option<&Point> {
    // Initializes the variables with the first point
    let mut min_d = points[0].dist(x, y);
    let mut min_p = Some(&points[0]);

    // Iterates over the points, skipping the first
    for p in points.iter().skip(1) {
        let d = p.dist(x, y);

        // If the distance is equals the the minimum already found, set the return value (min_p) to None
        if d == min_d {
            min_p = None;
        }
        // If the distance is less than the minimum already found, replace the temp variables with that
        else if d < min_d {
            min_d = d;
            min_p = Some(p);
        }
    }
    return min_p;
}

fn part1(points: &Vec<Point>) -> Result<usize, Box<Error>> {
    // Get the max x and y from the points
    let width = points.iter().max_by_key(|p| p.x).ok_or("No points")?.x;
    let height = points.iter().max_by_key(|p| p.y).ok_or("No points")?.y;

    // Simple 2D vector, initialized an option of the point closest to each (x, y)
    let mut grid = vec![None ; width * height];
    for x in 0..width {
        for y in 0..height {
            grid[x + width * y] = closest(points, x, y);
        }
    }

    // List of points that don't have an infinite region
    // Made by filtering out the one which region confines with the border
    let mut non_infinite_points = HashSet::<&Point>::from_iter(points.iter());
    for i in (0..width) // y = 0, min y
                .chain( (height-1)*width..height*width ) // y = height-1, max y
                .chain( (1..height-1).map(|i| width*i) ) // x = 0, min x
                .chain( (1..height-1).map(|i| width*i - 1) ) { // x = width-1, max x 
        if let Some(p) = grid[i] {
            non_infinite_points.remove(p);
        }
    }

    // Points of the non-infinite regions
    let countable_points: Vec<&Point> = grid
        .iter()
        .filter_map(|o|*o)
        .filter(|p| non_infinite_points.contains(p))
        .collect();

    // Return the max size of the non-infinite regions
    return Ok(non_infinite_points
        .iter()
        .map(|point| countable_points
            .iter()
            .filter(|countable_point| *countable_point == point)
            .count()
        )
        .max()
        .ok_or("Error")?);
}

fn part2(points: &Vec<Point>) -> Result<u32, Box<Error>> {
    // Get the max x and y from the points
    let width = points.iter().max_by_key(|p| p.x).ok_or("No points")?.x;
    let height = points.iter().max_by_key(|p| p.y).ok_or("No points")?.y;

    let mut count = 0;

    // Iterates over each point (x, y)
    for x in 0..width {
        for y in 0..height {
            // Calculates the sum of distances, and if less than 10000, add 1 to the count
            if points.iter().map(|p| p.dist(x, y)).sum::<usize>() < 10000 {
                count += 1;
            }
        }
    }

    return Ok(count);
}