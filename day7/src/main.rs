use std::collections::{BTreeMap, HashSet};
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let steps = parse_deps(&input).ok_or("Error parsing steps")?;

    println!("part1: {}", part1(steps.clone())?);
    println!("part2: {}", part2(steps.clone())?);

    return Ok(());
}

// Returns a sorted map with every step mapped to its dependencies
fn parse_deps(input: &str) -> Option<BTreeMap<char, Vec<char>>> {
    let mut steps = BTreeMap::new();
    let mut seen = HashSet::new();

    for line in input.lines() {
        // Get the steps ids
        let c1 = line.chars().nth(5)?;
        let c2 = line.chars().nth(36)?;

        // Add the dependency
        steps.entry(c2).or_insert(vec![]).push(c1);
        // Register the dependency id for later use
        seen.insert(c1);
    }

    // Add every step that was seen as dependency but not inserted before
    for c in seen {
        steps.entry(c).or_insert(vec![]);
    }

    return Some(steps);
}

fn part1(mut steps: BTreeMap<char, Vec<char>>) -> Result<String, Box<Error>> {
    let mut output = String::new();

    // Loop until every step is done
    while steps.len() != 0 {
        // Gets the first (alphabetically since deps is a sorted map) step that has no dependencies
        let first = *steps.iter().find(|(_, v)| v.len() == 0).ok_or("Cyclic dependencies!")?.0;
        // Adds the step to the output and removes it from the input
        output.push(first);
        steps.remove(&first);
        // Removes the step from the dependencies of the other steps
        for (_, deps) in steps.iter_mut() {
            if let Some(p) = deps.iter().position(|&r| r == first) {
                deps.remove(p);
            }
        }
    }

    return Ok(output);
}

fn part2(mut steps: BTreeMap<char, Vec<char>>) -> Result<u32, Box<Error>> {
    let mut workers: [Option<(char, u32)>; 5] = [None ; 5];
    let mut time = 0;

    loop {
        // Reduces the time left until a step is done
        // Removes steps that are done
        for option in workers.iter_mut() {
            if let Some((c, time_left)) = option {
                *time_left -= 1;
                if *time_left == 0 {
                    for (_, deps) in steps.iter_mut() {
                        if let Some(p) = deps.iter().position(|&r| r == *c) {
                            deps.remove(p);
                        }
                    }
                    *option = None;
                }
            }
        }

        // If there are free workers and steps with no dependencies, add them to the worker
        for option in workers.iter_mut() {
            if *option == None {
                if let Some((&c, _)) = steps.iter().find(|(_, v)| v.len() == 0) {
                    *option = Some((c, 60 + (c as u32 - 64)));
                    steps.remove(&c);
                }
            }
        }

        // Break is the workers are done
        if workers.iter().all(Option::is_none) {
            break;
        }

        time += 1;
    }

    return Ok(time);
}