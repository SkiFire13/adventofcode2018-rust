use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let root = Node::from(
        &mut input
            .lines()
            .nth(0)
            .ok_or("No lines in input")?
            .split(" ")
            .filter_map(|p| p.parse::<u32>().ok())
    )
    .ok_or("Error parsing the nodes")?;

    println!("part1: {}", part1(&root));
    println!("part2: {}", part2(&root));

    return Ok(());
}

// Local helper for a node
struct Node {
    childs: Vec<Node>,
    metadata: Vec<u32>
}

impl Node {
    fn from(iter: &mut Iterator<Item = u32>) -> Option<Self> {
        // First get the number of children and metadata
        let child_count = iter.next()?;
        let metadata_count = iter.next()?;

        // Get the children
        let mut childs = vec![];
        for _ in 0..child_count {
            childs.push(Self::from(iter)?);
        }
        // Get the metadata
        let mut metadata = vec![];
        for _ in 0..metadata_count {
            metadata.push(iter.next()?);
        }

        return Some(Self { childs, metadata });
    }

    // Sums the sum of metadata of itself with the one of its childs
    fn sum_metadata(&self) -> u32 {
        return self.metadata.iter().sum::<u32>() + self.childs.iter().map(Self::sum_metadata).sum::<u32>();
    }

    // Calculates the value of the node
    fn value(&self) -> u32 {
        // If the node doesn't have childs the value is the sum of the metadata
        if self.childs.len() == 0 {
            return self.metadata.iter().sum();
        }
        // Else the value is the sum of the values of the children with index specified in the metadata
        else {
            return self.metadata.iter().filter_map(|i| self.childs.get((*i - 1) as usize).map(Self::value)).sum();
        }
    }
}

fn part1(root: &Node) -> u32 {
    return root.sum_metadata();
}

fn part2(root: &Node) -> u32 {
    return root.value();
}