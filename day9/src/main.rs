use std::collections::VecDeque;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<Error>> {
    let input = read_to_string("./input/input")?;
    let (players, last_worth) = parse_input(&input)?;

    println!("part1: {}", part1(players, last_worth));
    println!("part2: {}", part2(players, last_worth));

    return Ok(());
}

// Parses the input and returns a tuple with the number of players and the last marble
fn parse_input(input: &str) -> Result<(u32, u32), Box<Error>> {
    let mut split = input.split_whitespace();
    let players: u32 = split.next().ok_or("InputError")?.parse()?;
    let last_worth: u32 = split.skip(5).next().ok_or("InputError")?.parse()?;
    return Ok((players, last_worth));
}

// Simulates a game and returns the score of the winner
fn winning_score(players: u32, last_worth: u32) -> u32 {
    let mut scores = vec![0u32 ; players as usize];
    let mut table = VecDeque::new(); // VecDeque to efficiently push back AND front
    table.push_back(0); // Initialize the table

    // Iterates over each marble
    for i in 1..(last_worth+1) {
        // If the marble is divisible by 23, the 7th marble on the left is removed, the score is added to the current player and the current marble is moved to the right one
        if i % 23 == 0 {
            table.rotate_right(7);
            scores[(i % players) as usize] += i + table.pop_back().unwrap();
            table.rotate_left(1);
        }
        // Else the current marble is moved to the right and the new one is added
        else {
            table.rotate_left(1);
            table.push_back(i);
        }
    }

    // Returns the max score, the winner's one
    return *scores.iter().max().unwrap();
}

fn part1(players: u32, last_worth: u32) -> u32 {
    return winning_score(players, last_worth);
}

fn part2(players: u32, last_worth: u32) -> u32 {
    return winning_score(players, last_worth * 100);
}
